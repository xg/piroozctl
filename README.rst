piroozctl -- Command-Line Interface for V2Ray Servers
=====================================================
*piroozctl* is a python3 script which does one thing and it is setting up your **Debian 11 (Bullseye)** server from the ground up to function as a V2Ray server.

piroozctl doesn't talk too much and does its job silently.

.. code-block:: bash

    # Tested on Debian 11
    $ wget https://codeberg.org/pirooz/piroozctl/raw/branch/main/piroozctl.py && 
    sudo python3 piroozctl.py --install-omnibus --single-server --domain sub.example.com && 
    cat vmess_link; echo

Target Users
============
The users in Iran are the main target users, but if someone else can also benefit from it, just drop a line in the issues, then we adapt it to you, also.

Dependencies
============
This script depends on the following free software:

- python3
- wget
- v2ray
- nginx
- certbot
- ufw
- linux (kernel)
- fail2ban

Reddit
======
Developers are hanging out sometimes on this subreddit: 

- https://www.reddit.com/r/pirooz/

License
=======
AGPLv3+
